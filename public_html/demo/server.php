<?php

require_once __DIR__ . '/../vendor/autoload.php';

use HemiFrame\Lib\WebSocket\Client;
use HemiFrame\Lib\WebSocket\WebSocket;

set_time_limit(0);

$socket = new WebSocket("localhost", 8010);
$socket->setEnableLogging(true);

$socket->on("receive", function (Client $client, $data) use ($socket) {
    foreach ($socket->getClientsByPath($client->getPath()) as $item) {
        /* @var $item \HemiFrame\Lib\WebSocket\Client */
//		if ($item->id != $client->id) {
        $socket->sendData($item, $data);
//		}
    }
});

$socket->on("error", function ($socket, $client, $phpError, $errorMessage, $errorCode) {
    var_dump("Error: => " . implode(" => ", $phpError));
});

$socket->startServer();
