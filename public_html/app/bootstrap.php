<?php
namespace gekradio;

use gekradio\core\App;
use gekradio\core\Route;
use gekradio\core\Session;
use gekradio\core\tools\l10n;

Session::getInstance();     // создаем сессию

Route::redirect2Alias();

l10n::init(App::settings('lang'));

//  >e[e!
Route::start();