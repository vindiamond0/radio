<?php
namespace gekradio\core\tools;

use gekradio\core\App;

class l10n
{
    static function init($lang = null)
    {
        if (is_null($lang))
            $lang = self::getLang();
        
        setcookie ("lang", $lang, time() + 3600*24*365, '/');

        $domain = "gekradio";
        switch($lang)
        {
            case "en":
                $locale = "en_US.UTF-8"; break;

            default:
                $lang = "ru";
                $locale = "ru_RU.UTF-8";
        }

        putenv("LANG=" . $locale);
        setlocale(LC_ALL, $locale);
        setlocale(LC_NUMERIC, 'C');
        bindtextdomain($domain, $_SERVER['DOCUMENT_ROOT'] . "/locale");
        textdomain($domain);
        bind_textdomain_codeset($domain, 'UTF-8');

        App::lang($lang);
    }

    // returns preselected or preferred language
    static function getLang()
    {
        $lang = null;

        if (!empty($_COOKIE['lang']) && in_array($_COOKIE['lang'], App::settings('siteLanguages')))
            $lang = $_COOKIE['lang'];

        if (!empty($_GET['lang']) && in_array($_GET['lang'], App::settings('siteLanguages')))
            $lang = $_GET['lang'];

        if (is_null($lang)) $lang = 'ru'; //Tools::getPrefLang();
        
        return $lang;
    }
}