<?php
namespace gekradio\core\tools;

use gekradio\core\App;

class Tools
{
    /**
     * альтернатива str_replace
     * @param $pairs
     *      ассоциативный массив "что" => "на что"
     * @param $string
     *      "где"
     */
    static function strReplace($pairs, $string)
    {
        return str_replace(array_keys($pairs), $pairs, $string);
    }

    static function getFileTree($dir = null)
    {
        // получаем список путей к файлам и папкам
        $dirContents = [['name' => 'music', 'parent' => -1]];

        self::getDirContents(App::settings('musicPath'), $dirContents);

        return $dirContents;
    }

    static function getDirContents($dir, &$results = [], $parent = 0)
    {
        $files = scandir($dir);

        foreach ($files as $key => $value)
        {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);

            if (!is_dir($path))
                $results[] = [
                    'name' => $value,
                    'parent' => $parent ];
            else
                if ($value != "." && $value != "..")
                {
                    $results[] = [
                        'name' => $value,
                        'parent' => $parent ];

                    self::getDirContents($path, $results, count($results) - 1);
                }
        }

        return $results;
    }

    // возвращает путь к файлу по его id
    static function getFilePathFromId($id)
    {
        $files = self::getFileTree();

        do
        {
            $file = $files[$id];
            $elements[] = $file['name'];
            $id = $file['parent'];
        } while ($id > 0);

        $elements = array_reverse($elements);

        return implode('/', $elements); //App::settings('musicPath') .
    }

    static function normalizeMetaTagData($metaData)
    {
        if (empty($metaData['Artist'])) {
            $title = explode(' - ', $metaData['basename'], 2)[0];
            $artist = explode(' - ', $metaData['basename'])[1];
            $metaData['Artist'] = explode('.', $artist)[0];
            $metaData['Title'] = $title;
        }

        return $metaData;
    }
}