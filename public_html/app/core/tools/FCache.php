<?php
/**
 * Файловый кеш
 * User: Дима
 * Date: 17.08.2016
 * Time: 11:52
 */

namespace gekradio\core\tools;

class FCache
{
    // глобальное включение/отключение кеширования
    const CacheEnabled = true;

    // папка хранения фалов кеша
    const cachePath = '/app/core/config/';

    /**
     * Возвращает значение если оно сохранено и время жизни не истекло, иначе null
     *
     * @param $key
     * @return bool
     */
    static function getValue($key)
    {
        if (!self::CacheEnabled)
            return null;

        $fileName = $_SERVER['DOCUMENT_ROOT'] . self::cachePath . $key . ".json";

        if (!file_exists($fileName))
            return null;

        $fileData = json_decode((string) file_get_contents($fileName), true);

        return !empty($fileData['expires']) && $fileData['expires'] < time()
            ? null
            : $fileData['data'];
    }

    /**
     * @param $key
     * @param $value
     * @param int $lifeMinutes    срок жизни в минутах
     * @param bool $rename
     * @return bool|int
     */
    static function saveValue($key, $value, $lifeMinutes = 0, $rename = false)
    {
        if (!self::CacheEnabled)
            return false;

        $fileName = $_SERVER['DOCUMENT_ROOT'] . self::cachePath . $key . ".json";

        if (file_exists($fileName) && $rename)
            rename($fileName, $_SERVER['DOCUMENT_ROOT'] . self::cachePath . time() . $key . ".bak" );

        // добавляем срок жизни в часах
        $expires = new \DateTime();
        $expires->modify('+' . $lifeMinutes . ' minutes');

        return file_put_contents(
            $fileName,
            json_encode([
                'expires' => empty($lifeMinutes) ? '' : $expires->getTimestamp(),
                'data' => $value ]));
    }

    // удаляет ключ
    static function deleteKey($key)
    {
        if (!self::CacheEnabled)
            return false;

        $fileName = $_SERVER['DOCUMENT_ROOT'] . self::cachePath . $key . ".json";

        return file_exists($fileName)
            ? unlink($fileName)
            : false;
    }

    // удаляет все ключи
    static function flushAll()
    {
        if (!self::CacheEnabled)
            return [
                "success" => false,
                "descr" => "<p style='font-size: 24px; color: red;'>Файловый кеш не очищен!</p>"];

        $fileMask = $_SERVER['DOCUMENT_ROOT'] . self::cachePath . "*.json";
        $count = 0;

        foreach (glob($fileMask) as $fileName)
        {
            $newFileName = rtrim($fileName, '.json') . "_" . time() . ".bak";

            if (rename($fileName, $newFileName ))
                ++$count;
        }

        return [
            "success" => true,
            "msg" => "<p style='font-size: 24px; color: green;'>{$count} файлов было удалено.</p>"];
    }
}