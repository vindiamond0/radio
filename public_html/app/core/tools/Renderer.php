<?php
namespace gekradio\core\tools;

use gekradio\core\App;
use gekradio\core\Settings;

class Renderer
{
    static function printJs()
    {
        $result = "";

        foreach ((array) App::settings('js') as $js)
            if (!empty($js))
                $result .= strpos($js, 'http') === 0 || strpos($js, '//') === 0
                    ? "<script type='text/javascript' src='{$js}'></script>\n"
                    : "<script type='text/javascript' src='" . App::path2Assets() . "js/{$js}?v=" . Settings::getInstance()->settings('assetsVersion') . "'></script>\n";

        return $result;
    }

    static function printCss()
    {
        $result = "";

        foreach ((array) App::settings('css') as $css)
            if (!empty($css))
                $result .= strpos($css, 'http') === 0 || strpos($css, '//') === 0
                    ? "<link rel='stylesheet' type='text/css' href='{$css}' />\n"
                    : "<link rel='stylesheet' type='text/css' href='" . App::path2Assets() . "css/{$css}?v=" . Settings::getInstance()->settings('assetsVersion') . "' />\n";

        return $result;
    }

    // возвращает разметку для дерева файлов
    static function genCategoriesTree($tree, $parent = 0)
    {
        $markup = "";

        foreach ($tree as $key=>$item)
            if ($item['parent'] == $parent)
            {
                $submenu = self::genCategoriesTree($tree, $key);

                $markup .= "<li id='{$key}' " . (empty($submenu) ? '' : "class='folder'") . ">{$item['name']}"
                    . "{$submenu}"
                    . "</li>";
            }

        return empty($markup)
            ? ""
            : "<ul " . (empty($parent) ? " id='treeData' style='display: none;'" : "") . ">{$markup}</ul>";
    }
}