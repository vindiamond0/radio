<?php
namespace gekradio\core\tools;

use gekradio\core\App;
use Monolog\Handler\NativeMailerHandler;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;

class Log
{
    protected static $instance;

    /**
     * Method to return the Monolog instance
     *
     * @return \Monolog\Logger
     */
    static public function getLogger()
    {
        if (!self::$instance)
            self::configureInstance();

        return self::$instance;
    }

    /**
     * Configure Monolog to use a rotating files system.
     *
     * @return Logger
     */
    protected static function configureInstance()
    {
        $dir = $_SERVER['DOCUMENT_ROOT'] . '/log/monolog';

        if (!file_exists($dir))
            mkdir($dir, 0777, true);

        $logger = new Logger('JG');
        $logger->pushHandler(new RotatingFileHandler($dir . '/main.log', 50));
        $logger->pushHandler(new NativeMailerHandler('vindiamond0@gmail.com', 'Warning', 'GekRadio'));

        self::$instance = $logger;
    }

    public static function debug($message, array $context = array())
    {
        self::getLogger()->addDebug($message, $context);
    }

    public static function info($message, array $context = array())
    {
        self::getLogger()->addInfo($message, $context);
    }

    public static function warning($message, array $context = array())
    {
        self::getLogger()->addWarning($message, $context);
    }

    public static function error($message, array $context = array())
    {
        self::getLogger()->addError($message, $context);
    }

    public static function critical($message, array $context = array())
    {
        self::getLogger()->addCritical($message, $context);
    }

    public static function alert($message, array $context = array())
    {
        self::getLogger()->addAlert($message, $context);
    }

    public static function emergency($message, array $context = array())
    {
        self::getLogger()->addEmergency($message, $context);
    }
}