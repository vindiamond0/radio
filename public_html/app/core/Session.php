<?php
namespace gekradio\core;

class Session
{
    protected static $_instance = null;

    protected $_session;

    // приватный конструктор ограничивает реализацию getInstance ()
    private function __construct()
    {
        $this->startSession();
    }

    // ограничивает клонирование объекта
    protected function __clone()
    {
    }

    static public function getInstance()
    {
        if (is_null(self::$_instance))
            self::$_instance = new self();

        return self::$_instance;
    }

    public function import()
    {
    }

    public function get()
    {
    }

    private function startSession()
    {
        session_start();
    }

    function destroySession()
    {
        session_destroy();
    }

    function getLoggedUser()
    {
        return [
            'id' => $this->getParam('user')['id'],
            'role' => $this->getParam('user')['role'],
            'name' => $this->getParam('user')['name'],
            'email' => $this->getParam('user')['email'] ];
    }

    function writeUser($params)
    {
        foreach ($params as $key=>$val)
            $_SESSION['user'][$key] = $val;
    }

    function getParam($key)
    {
        return empty($_SESSION[$key])
            ? null
            : $_SESSION[$key];
    }

    function writeParam($key, $val)
    {
        $_SESSION[$key] = $val;
    }
}