<?php
namespace gekradio\core;

class App
{
    static function lang($lang = NULL)
    {
        return self::settings('lang', $lang);
    }

    static function path2Assets()
    {
        return self::settings('pathToAssets');
    }

    /*
     * Gets | Sets sites settings params
     */
    static function settings($param, $value = null)
    {
        $settings = Settings::getInstance();

        if (is_null($value))
            return $settings->settings($param);
        else
            $settings->settings($param, $value);
        
        return '';
    }
}