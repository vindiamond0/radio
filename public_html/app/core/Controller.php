<?php
namespace gekradio\core;

class Controller
{
    public $model;
    public $view;
    public $post;

    function __construct()
    {
        parse_str(file_get_contents('php://input'), $this->post);

        $this->view = new View();
    }

    // добавляет скрипт для вывода на странице
    function addJs($jsName)
    {
        App::settings(
            'js',
            array_merge(
                (array) App::settings('js'),
                (array) $jsName ));
    }

    // добавляет стили для вывода на странице
    function addCss($css)
    {
        App::settings(
            'css',
            array_merge(
                (array) App::settings('css'),
                (array) $css ));
    }
}