<?php
namespace gekradio\core;

class Settings
{
    // настройки сайта
    private $settings = [
        'siteLanguages' => ['ru', 'en'],
        'lang'          => 'ru',
        'hostName'      => '//gekradio.com/',
        'pathToAssets'  => '//gekradio.com/assets/',
        'musicPath'     => '/home/johngek/domains/gekradio.com/music/',
        'controller'    => '',
        'action'        => '',
        'title'         => '',
        'assetsVersion' => 10052,
        'description'   => "",
        'keywords'      => "",
        'devEmails'     => [ 'vindiamond0@gmail.com' ],
        'js'            => [],
        'css'           => [ 'main.min.css' ]];


    private static $_instance = null;

    // приватный конструктор ограничивает реализацию getInstance ()
    private function __construct()
    {
    }

    // ограничивает клонирование объекта
    protected function __clone()
    {
    }

    static public function getInstance()
    {
        if (is_null(self::$_instance))
            self::$_instance = new self();

        return self::$_instance;
    }

    public function import()
    {
    }

    public function get()
    {
    }

    // текущее значение параметра настройки сайта
    public function settings($param, $value = null)
    {
        return is_null($value)
            ? $this->settings[$param]
            : ($this->settings[$param] = $value);
    }
}