<?php
namespace gekradio\core;

class View
{
    function generate($content_view, $template_view, $params = null)
    {
        include $_SERVER['DOCUMENT_ROOT'] . '/app/views/' . ($template_view ?: 'layout.php');
    }
}