<?php
namespace gekradio\core;

use gekradio\controllers\MainController;

class Route
{
    static function start()
    {
        // получаем имя контроллера и экшна
        $controller_name = empty($_GET['page']) ? 'main' : strtolower($_GET['page']);
        $action_name = empty($_GET['action']) ? 'index' : strtolower($_GET['action']);

        // сохраняем в настройках название контроллера и экшна вызываемой страницы
        App::settings('controller', $controller_name);
        App::settings('action', $action_name);

        // добавляем префиксы
        $model_name = ucfirst($controller_name);
        $controller_name = ucfirst($controller_name) . 'Controller';
        $action_name = 'action_'.$action_name;

        // подцепляем файл с классом модели (файла модели может и не быть)
        $model_file = ucfirst($model_name) . '.php';
        $model_path = $_SERVER['DOCUMENT_ROOT'] . "/app/models/" . $model_file;
        if (file_exists($model_path))
            include $model_path;

        // подцепляем файл с классом контроллера
        $controller_file = $controller_name.'.php';
        $controller_path = $_SERVER['DOCUMENT_ROOT'] . "/app/controllers/" . $controller_file;

        if (!file_exists($controller_path))
            return Route::ErrorPage404(); // правильно было бы кинуть здесь исключение

        // создаем контроллер
        $controller_name = "gekradio\\controllers\\{$controller_name}";
        $controller = new $controller_name;

        if (method_exists($controller, $action_name))
            $controller->$action_name(); // вызываем действие контроллера
        else
            Route::ErrorPage404(); // здесь также разумнее было бы кинуть исключение
    }

    /**
     * определяет адрес страницы по её алиасу
     */
    static function redirect2Alias()
    {
        // обрезаем параметры
        $uriParts = explode('?', ltrim($_SERVER['REQUEST_URI'], '/'));

        if (empty($uriParts[0]) || $uriParts[0] == 'app.php')
            return;

        $scriptNameParts = explode('/', $uriParts[0]);

        // определяем и убираем из урла язык
        if (in_array($scriptNameParts[0], App::settings('siteLanguages')))
        {
            App::settings('lang', $scriptNameParts[0]);

            unset($scriptNameParts[0]);

            $scriptNameParts = array_values($scriptNameParts);
        }

        $_GET['action'] = empty($scriptNameParts[1])
            ? 'index'
            : $scriptNameParts[1];

        if (is_numeric($_GET['action']))
        {
            $_GET['id'] = $_GET['action'];
            $_GET['action'] = 'index';
        }

        // если последний параметр в url числовой, то это id
        if (is_numeric($scriptNameParts[count($scriptNameParts) - 1]))
            $_GET['id'] = $scriptNameParts[count($scriptNameParts) - 1];

        $_GET['page'] = $scriptNameParts[0];
    }

    static function ErrorPage404()
    {
        header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found", true, 404);
        header("Status: 404 Not Found");

        $mainController = new MainController();
        return $mainController->action_404();
    }
}