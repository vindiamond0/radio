<?php
namespace gekradio\controllers;

use gekradio\core\Controller;
use gekradio\core\tools\FCache;
use gekradio\core\tools\Tools;
use gekradio\models\Covers;
use gekradio\models\History;
use gekradio\models\MpdWrpr;

class AjaxController extends Controller
{
    function action_add()
    {
        echo json_encode(MpdWrpr::getInstance()->playlist_add(Tools::getFilePathFromId($this->post['id']))
            ? [ 'success' => true ]
            : [ 'success' => false,
                'descr' => MpdWrpr::getInstance()->get_error() ]);
    }

    function action_clearpl()
    {
        echo json_encode(MpdWrpr::getInstance()->playlist_clear()
            ? [ 'success' => true ]
            : [ 'success' => false,
                'descr' => MpdWrpr::getInstance()->get_error() ]);
    }

    function action_getNowPlaying()
    {
        $curSong = MpdWrpr::getInstance()->current_song();

        if (!empty($curSong['Title']))
            $data['title'] = $curSong['Title'];

        if (!empty($curSong['Artist']))
            $data['artist'] = $curSong['Artist'];

        if (!empty($curSong['Album']))
            $data['album'] = $curSong['Album'];

        $data['cover'] = Covers::getCoverUrl(
            $data['artist'] ?? '',
            $data['album'] ?: ($data['title'] ?: '') );

        $serverStatus = MpdWrpr::getInstance()->server_status();
        $data['rest'] = (int)($curSong['Time'] - $serverStatus['elapsed']);

        echo json_encode([
            'success' => true,
            'data' => $data ]);
    }

    function action_getHistory()
    {
        History::saveTrack();

        $history = (array) FCache::getValue('history');

        echo json_encode(empty($history)
            ? [ 'success' => false ]
            : [ 'success' => true,
                'tracks' => array_reverse($history) ]);
    }

    function action_getList()
    {
        $list = MpdWrpr::getInstance()->playlist();
        $status = MpdWrpr::getInstance()->server_status();

        echo json_encode( !empty($list)
            ? [ 'success' => true,
                'list' => $list,
                'status' => $status ]
            : [ 'success' => false,
                'descr' => MpdWrpr::getInstance()->get_error() ]);
    }

    function action_play()
    {
        echo json_encode(MpdWrpr::getInstance()->play(0)
            ? [ 'success' => true ]
            : [ 'success' => false,
                'descr' => MpdWrpr::getInstance()->get_error() ]);
    }

    function action_random()
    {
        $state = MpdWrpr::getInstance()->random();

        echo json_encode( is_null($state)
            ? [ 'success' => false,
                'descr' => MpdWrpr::getInstance()->get_error() ]
            : [ 'success' => true,
                'state' => $state ] );
    }

    function action_repeat()
    {
        $state = MpdWrpr::getInstance()->repeat();

        echo json_encode( is_null($state)
            ? [ 'success' => false,
                'descr' => MpdWrpr::getInstance()->get_error() ]
            : [ 'success' => true,
                'state' => $state ] );
    }

    function action_stop()
    {
        echo json_encode(MpdWrpr::getInstance()->stop()
            ? [ 'success' => true ]
            : [ 'success' => false,
                'descr' => MpdWrpr::getInstance()->get_error() ]);
    }
}