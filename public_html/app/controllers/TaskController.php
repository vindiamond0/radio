<?php
namespace gekradio\controllers;

use gekradio\models\History;
use gekradio\models\MpdWrpr;

//use gekradio\models\Schedule;

class TaskController extends \dbObject
{
    /**
     * Collects playlist history
     */
    function action_logtrack()
    {
        echo History::saveTrack()
            ? 'stored'
            : 'already exists';
    }

    function action_switchplaylist()
    {
        // TODO если текущий плейлист отличается от нужного сейчас и до конца песни меньше минуты,
        // ждём и переключаем плейлист
        $currentPlId = MpdWrpr::getInstance()->server_status()['playlist'];

        var_dump($currentPlId);

        var_dump(MpdWrpr::getInstance()->server_stats());
    }
}