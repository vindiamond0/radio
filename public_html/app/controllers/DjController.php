<?php
namespace gekradio\controllers;

use gekradio\core\App;
use gekradio\core\Controller;
use gekradio\core\tools\Renderer;
use gekradio\core\tools\Tools;
use gekradio\models\MpdWrpr;
use gekradio\models\Schedule;

class DjController extends Controller
{
    function __construct()
    {
        if (!isset($_COOKIE['iamjohn']) && App::settings('action') !== 'iamjohn')
            die('Access denied');

        App::settings('css', []);

        $this->addJs('djmain.js');

        parent::__construct();
    }

    function action_index()
    {
        $this->addCss([
            App::path2Assets() . 'jquery-ui-1.12.1/jquery-ui.css',
            'ui.fancytree.min.css']);

        $this->addJs([
            App::path2Assets() . 'jquery-ui-1.12.1/jquery-ui.js',
            'jquery.fancytree-all.min.js',
            'extensions/jquery.fancytree.childcounter.js',
            'extensions/jquery.fancytree.columnview.js',
            'extensions/jquery.fancytree.dnd.js',
            'extensions/jquery.fancytree.edit.js',
            'extensions/jquery.fancytree.filter.js']);

        MpdWrpr::getInstance()->update_db();

//        $server_status = [
//            "volume" => "100",
//            "repeat" => "0",
//            "random" => "0",
//            "single" => "0",
//            "consume" => "0",
//            "playlist" => "2",
//            "playlistlength" => "0",
//            "mixrampdb" => "0.000000",
//            "state" => "stop",
//            "updating_db" => "55" ];

        $this->view->generate("dj.php", "djlayout.php", [
            "pageName" => "MainPage",
            'dataProvider' => [
                'list' => MpdWrpr::getInstance()->playlist(),
                'markup' => Renderer::genCategoriesTree(Tools::getFileTree()),
                'status' => MpdWrpr::getInstance()->server_status(),
                'playlists' => Schedule::getSchedule()]]);
    }

    function action_iamjohn()
    {
        setcookie ("iamjohn", 'yesiam', time() + 3600*24*365, '/');
        $_COOKIE['iamjohn'] = 'yesiam';

        header("HTTP/1.1 301 Moved Permanently");
        header("Location: http://gekradio.com/dj");
        exit();
    }
}