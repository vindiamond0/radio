<?php
namespace gekradio\controllers;

use gekradio\core\Controller;
use gekradio\core\tools\Tools;
use gekradio\models\Covers;
use gekradio\models\MpdWrpr;

class MainController extends Controller
{
    function action_404()
    {
        $this->view->generate("info_page.php", "", [
            "pageName" => "404 " . _("Page not found") . " :(",
            'dataProvider' => [
                'text' => sprintf(_("Back to %smain page%s."), "<a href='/'>", "</a>") ] ]);
    }

    function action_index()
    {
        $this->addJs('main.js');

        MpdWrpr::getInstance()->update_db();

        $server_status = [
            "volume" => "100",
            "repeat" => "0",
            "random" => "0",
            "single" => "0",
            "consume" => "0",
            "playlist" => "2",
            "playlistlength" => "0",
            "mixrampdb" => "0.000000",
            "state" => "stop",
            "updating_db" => "55" ];

        $curentSong = [
            ["type" => "file",
            "name" => "GEKRADIO ВСЕ КУЧЕЙ  MP3 001 Var.2/Toxic - Melanie Martinez.mp3",
            "basename" => "Toxic - Melanie Martinez.mp3",
            "Last-Modified" => "2018-09-20T12:55:34Z",
            "Artist" => "Melanie Martinez",
            "Title" => "Toxic (The Voice Performance)",
            "Time" => "232",
            "Pos" => "108",
            "Id" => "812" ]];


        $this->view->generate("main.php", "", [
            "pageName" => "MainPage",
            'dataProvider' => [ ]]);
    }

    function action_test()
    {
        //$nowPlaying = MpdWrpr::getInstance()->current_song();

        //var_dump(Tools::normalizeMetaTagData($nowPlaying));

        https://itunes.apple.com/search?term=Madness&album=Death+Of+A+Rude+Boy&media=music&entity=musicTrack&limit=1
        var_dump(Covers::getCoverUrl(
            'Madness',
            'Death Of A Rude Boy' ));

        exit();

        //  AIzaSyBGIrW0nbkz5gANk0WPLiiRPC_CE783Owk
//        $result = file_get_contents('https://www.shazam.com/discovery/v2/ru/UA/web/-/search?searchQuery=a wish for you - native&pageSize=3&startFrom=0&ios84:true');
//        $result = file_get_contents('https://cse.google.com/cse?cx=013413131088219418869:xfraa_fd3ne');
        //$result = file_get_contents('https://www.googleapis.com/customsearch/v1?key=AIzaSyBGIrW0nbkz5gANk0WPLiiRPC_CE783Owk&cx=013413131088219418869:xfraa_fd3ne&q=a+wish+for+you+(native)');

        //var_dump($result);
        //var_dump(MpdWrpr::getInstance()->curent_song());
        //var_dump(MpdWrpr::getInstance()->playlist());

        //https://affiliate.itunes.apple.com/resources/documentation/itunes-store-web-service-search-api/

        $data = file_get_contents("https://itunes.apple.com/search?&term=Loungeclash&album=TDread+Time+Story&media=music&entity=musicTrack&limit=1");

        $dec = json_decode($data, true);

        var_dump(empty($dec["results"][0]['artworkUrl100']) ? 'no' : $dec["results"][0]['artworkUrl100']);
    }
}