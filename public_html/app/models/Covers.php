<?php
namespace gekradio\models;

use gekradio\core\tools\Log;
use gekradio\core\tools\Tools;

class Covers
{
    static function getCoverUrl($artist, $album)
    {
        if (empty($artist))
            return '/app/core/config/image.jpg';

        $fileName = preg_replace("/[^\w\-\.]/", "", Tools::strReplace(
            [   " " => "_" ],
            strtolower($artist . "_" . $album . ".jpg") ));

        //Log::debug('fileName', ['script' => 'Covers::getCoverUrl', 'fileName' => $fileName, 'artist' => $artist, 'album' => $album ]);

        if (file_exists(__DIR__ . "/../core/config/" . $fileName))
            return '/app/core/config/' . urlencode($fileName);

        $request = 'term=' . urlencode($artist);

        if (in_array(strtolower($artist), ['madness']))
            $request .= '+' . urlencode($album);

        // получаем урл обложки
        $data = file_get_contents("https://itunes.apple.com/search?{$request}&media=music&entity=musicTrack&limit=1");
        //$data = file_get_contents("https://itunes.apple.com/search?{$request}&attribute=artistTerm&media=music&entity=musicTrack&limit=1");

        Log::debug('itunes data', [
            'script' => 'Covers::getCoverUrl',
            'url' => "https://itunes.apple.com/search?{$request}&media=music&entity=musicTrack&limit=1",
            'data' => $data ]);

        $dec = json_decode($data, true);

//        $dec = [
//            "resultCount" => 1,
//            "results" =>  [
//                [
//                    "wrapperType" => "track",
//                    "kind" => "song",
//                    "artistId" => 6596751,
//                    "collectionId" => 201257489,
//                    "trackId" => 201257673,
//                    "artistName" => "Franz Ferdinand",
//                    "collectionName" => "Franz Ferdinand",
//                    "trackName" => "Take Me Out",
//                    "collectionCensoredName" => "Franz Ferdinand",
//                    "trackCensoredName" => "Take Me Out",
//                    "artistViewUrl" => "https://itunes.apple.com/us/artist/franz-ferdinand/6596751?uo=4",
//                    "collectionViewUrl" => "https://itunes.apple.com/us/album/take-me-out/201257489?i=201257673&uo=4",
//                    "trackViewUrl" => "https://itunes.apple.com/us/album/take-me-out/201257489?i=201257673&uo=4",
//                    "previewUrl" => "https://audio-ssl.itunes.apple.com/apple-assets-us-std-000001/Music/84/ea/97/mzm.tfhjedfp.aac.p.m4a",
//                    "artworkUrl30" => "https://is1-ssl.mzstatic.com/image/thumb/Music/v4/8b/91/c8/8b91c854-a0a4-7309-8afb-b8dbfedd2fc1/source/30x30bb.jpg",
//                    "artworkUrl60" => "https://is1-ssl.mzstatic.com/image/thumb/Music/v4/8b/91/c8/8b91c854-a0a4-7309-8afb-b8dbfedd2fc1/source/60x60bb.jpg",
//                    "artworkUrl100" => "https://is1-ssl.mzstatic.com/image/thumb/Music/v4/8b/91/c8/8b91c854-a0a4-7309-8afb-b8dbfedd2fc1/source/100x100bb.jpg",
//                    "collectionPrice" => 9.99,
//                    "trackPrice" => 1.29,
//                    "releaseDate" => "2004-01-12T08:00:00Z",
//                    "collectionExplicitness" => "notExplicit",
//                    "trackExplicitness" => "notExplicit",
//                    "discCount" => 1,
//                    "discNumber" => 1,
//                    "trackCount" => 11,
//                    "trackNumber" => 3,
//                    "trackTimeMillis" => 237024,
//                    "country" => "USA",
//                    "currency" => "USD",
//                    "primaryGenreName" => "Alternative",
//                    "isStreamable" => true]]
//        ];

        // если обложка не найдена, то копируем заглушку
        empty($dec["results"][0]['artworkUrl100'])
            ? copy(__DIR__ . "/../core/config/image.jpg", __DIR__ . "/../core/config/" . $fileName)
            : file_put_contents(__DIR__ . "/../core/config/" . $fileName, file_get_contents($dec["results"][0]['artworkUrl100']));

        return "/app/core/config/" . urlencode($fileName);

        //Artist: Little Jackie
        //Album: Nova Tunes 1.8


    }
}