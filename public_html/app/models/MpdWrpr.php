<?php
namespace gekradio\models;

use gekradio\core\tools\MPD;

class MpdWrpr
{
    private static $_instance = null;

    private $mpd;

    // приватный конструктор ограничивает реализацию getInstance ()
    private function __construct()
    {
        $this->mpd = new MPD('gekradio.com', 6600);
    }

    // ограничивает клонирование объекта
    protected function __clone()
    {
    }

    static function getInstance()
    {
        if (is_null(self::$_instance))
            self::$_instance = new self();

        return self::$_instance;
    }

    public function current_song()
    {
        $metaData = $this->mpd->current_song()[0];

        if (empty($metaData['Artist'])) {
            $title = explode(' - ', $metaData['basename'], 2)[0];
            $artist = explode(' - ', $metaData['basename'])[1];
            $metaData['Artist'] = explode('.', $artist)[0];
            $metaData['Title'] = $title;
        }

        return $metaData;
    }

    public function get_error()
    {
        return $this->mpd->get_error();
    }

    public function play($id)
    {
        return $this->mpd->play($id);
    }

    public function playlist()
    {
        return $this->mpd->playlist();
    }

    public function playlist_add($uri)
    {
        return $this->mpd->playlist_add($uri);
    }

    public function playlist_clear()
    {
        return $this->mpd->playlist_clear();
    }

    public function random()
    {
        return $this->mpd->random();
    }

    public function repeat()
    {
        return $this->mpd->repeat();
    }

    public function server_status()
    {
        return $this->mpd->server_status();
    }

    public function server_stats()
    {
        return $this->mpd->playlists();
    }

    public function stop()
    {
        return $this->mpd->stop();
    }

    public function update_db($uri = '')
    {
        return $this->mpd->update_db($uri);
    }
}