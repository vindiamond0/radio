<?php
namespace gekradio\models;

use gekradio\core\tools\FCache;
use gekradio\core\tools\Log;

class History
{
    const historyLength = 10;   // tracks in history

    static function saveTrack()
    {
        $history = (array) FCache::getValue('history');

        $lastSong = empty($history)
            ? []
            : $history[count($history) - 1];

        $nowPlaying = MpdWrpr::getInstance()->current_song();

        $serverStatus = MpdWrpr::getInstance()->server_status();
        $elapsed = (int) $serverStatus['elapsed'];

        // if the track has been already logged, then stop...
        if (!empty($lastSong['title']) && $lastSong['title'] == $nowPlaying['Title'])
            return false;

        Log::debug('playing now', [
            'title' => $lastSong['title'],
            'started' => $lastSong['started'],
            'pos' => $elapsed,
            'calc' => $lastSong['started'] - strtotime("-{$elapsed} second") ]);

        if (empty($lastSong['title']) && abs($lastSong['started'] - strtotime("-{$elapsed} second")) < 5)
            return false;

        $history[] = [
            'title' => $nowPlaying['Title'] ?? '',
            'artist' => $nowPlaying['Artist'] ?? '',
            'album' => $nowPlaying['Album'] ?? '',
            'started' => strtotime("-{$elapsed} second"),
            'cover' => Covers::getCoverUrl(
                $nowPlaying['Artist'] ?: '',
                $nowPlaying['Album'] ?: ($nowPlaying['Title'] ?: '') ) ];

        return FCache::saveValue('history', array_slice($history, -self::historyLength));
    }
}