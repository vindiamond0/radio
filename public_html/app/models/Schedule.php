<?php

namespace gekradio\models;

class Schedule
{
    /**
     * returns name of playlist scheduled to play at this moment
     * @return string
     */
    static function getPlayList()
    {
        $schedule = Schedule::getSchedule();

        $result = end($schedule);

        foreach (array_keys($schedule) as $key => $val)
            if (date("Hi") >= $val)
                $result = array_values($schedule)[$key];

        return $result;
    }

    static function getSchedule()
    {
        return [
            '600' => 'dayPlayList',
            '2300' => 'nightPlayList'];
    }
}