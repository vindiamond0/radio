<?php
namespace gekradio\views;

use gekradio\core\App;
use gekradio\core\tools\Renderer;
?>

<!DOCTYPE html>
<html lang="<?=App::lang()?>">
<head>
    <title><?=_("GekRadio.com")?> | <?=strip_tags($params['pageName'])?></title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="description" content="<?=App::settings('description')?>">
    <meta name="keywords" content="<?=App::settings('keywords')?>" />

    <link rel="shortcut icon" href="<?=App::path2Assets()?>/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?=App::path2Assets()?>/images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <?= Renderer::printCss(); ?>

</head>
<body>
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">GekRadio.com</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <button id="play" class="btn btn-primary"
                                style="<?=$params['dataProvider']['status']['state'] === 'stop' ? '' : 'display: none'?>">
                            <i class="fas fa-play"></i></button>
                        <button id="stop" class="btn btn-light"
                                style="<?=$params['dataProvider']['status']['state'] === 'stop' ? 'display: none' : ''?>">
                            <i class="fas fa-stop"></i></button>
                    </li>
                    <li class="nav-item">
                        <button id="repeat" class="btn <?=$params['dataProvider']['status']['repeat'] ? 'btn-light' : 'btn-primary'?>">
                            <i class="fas fa-redo"></i></button>
                    </li>
                    <li class="nav-item">
                        <button id="random" class="btn <?=$params['dataProvider']['status']['random'] ? 'btn-light' : 'btn-primary'?>">
                            <i class="fas fa-random"></i></button>
                    </li>
                    <li class="nav-item">
                        <button id="trash" class="btn btn-warning">
                            <i class="fas fa-trash-alt"></i></button>
                    </li>
                </ul>
                <form class="form-inline">
                    <select class="form-control mr-sm-2" onchange="switchpl(this.value)">
                        <? foreach ($params['dataProvider']['playlists'] as $key => $value) { ?>
                            <option value="<?= $key ?>"><?= "(" . substr("0{$key}", -4, 2) . ":" . substr($key, -2) . ") " . $value ?></option>
                        <? } ?>
                    </select>
                </form>
            </div>
        </nav>

        <? include 'app/views/' . $content_view;?>

    </div>


<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
<script src="<?=App::path2Assets() . 'js/jquery-3.3.1.min.js'?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>

<?= Renderer::printJs(); ?>

</body>
</html>
