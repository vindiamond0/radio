<?php
namespace gekradio\views;
?>

<div class="row" style="display: none;">
    <div class="col-md-2"></div>
    <div class="col-md-8 bnnr-728x90">728 x 90</div>
    <div class="col-md-2"></div>
</div>

<!--<div class="row mrgn">-->
<!--    <div class="col-md-6"><h1>Gekradio.com</h1></div>-->
<!--    <div class="col-md-6"></div>-->
<!--</div>-->

<div class="row mrgn">
    <div class="col-md-12">
        <img id="logo" src="/assets/images/logo.png">
    </div>
    <div class="col-md-6">
        <div id="nwplng"></div>
        <audio style="display: block;" controls autoplay="autoplay">
            <source src="http://gekradio.com:8000/mpd?type=.mp3" type="audio/mp3">Your browser does not support the audio element.
        </audio>
        <div class="clearfix"></div>
        <div id="history"></div>
    </div>
    <div class="col-md-6" style="margin: -10px 0 0 0">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/zQ-09tk986M" frameborder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen></iframe>
    </div>
</div>