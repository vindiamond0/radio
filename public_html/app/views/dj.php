<?php
namespace gekradio\views;
?>
<style type="text/css">
    .time {
        color: #26A0DA;
    }
</style>

    <div class="row">
        <div class="col-md-8">
            <p id="playList"></p>
        </div>
        <div class="col-md-4">
            <div id="tree">
                <ul id="treeData" style="display: none;"></ul>
            </div>
        </div>
    </div>

<script type="application/javascript">
    let markup = "<?=$params['dataProvider']['markup']?>";
</script>