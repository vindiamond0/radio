const echo = console.log;

$(function () {
    nowPlaying();
});

function nowPlaying() {
    fetch('/ajax/getNowPlaying', {
        method: 'post',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        }
    })
        .then(function (r) {
            return r.ok
                ? Promise.resolve(r)
                : Promise.reject(new Error(r.statusText))
        })
        .then(r => r.json())
        .then(function (data) {
            if (!data.success)
                return Promise.reject(new Error(data.descr));

            let html = "<div class='main-glass'></div>";

            interrupt = data.data.rest > 180 ? 60 * 1000 : (data.data.rest + 1) * 1000;

            if (interrupt < 10000)
                interrupt = 10000;

            if (data.data.cover)
                html += "<img src='" + data.data.cover + "'>";

            if (data.data.title)
                html += 'Track: ' + data.data.title + '<br>';

            if (data.data.artist)
                html += 'Artist: ' + data.data.artist + '<br>';

            if (data.data.album)
                html += 'Album: ' + data.data.album + '<br>';

            $('#nwplng').html(html);

            echo('wait: ' + interrupt);

            getHistory();

            setTimeout(nowPlaying, interrupt);

            echo('done: ' + interrupt);
        })
        .catch(function (error) {
            console.error('Request failed', error);
        });
}

function getHistory() {
    $.ajax({
        type: 'POST',
        url: '/ajax/getHistory',
        success: function (data) {
            obj = data ? JSON.parse(data) : {};

            let html = '<br>';

            if (obj && obj.success) {
                for (let track in obj.tracks)
                    if (obj.tracks.hasOwnProperty(track)) {
                        let date = new Date();

                        date.setTime(obj.tracks[track].started * 1000);

                        html += "<div class='glass'></div>";
                        html += myFormat(date.getHours()) + ':' + myFormat(date.getMinutes()) + '<br>';

                        if (obj.tracks[track].cover)
                            html += "<img src='" + obj.tracks[track].cover + "'>";

                        if (obj.tracks[track].title)
                            html += 'Track: ' + obj.tracks[track].title + '<br>';

                        if (obj.tracks[track].artist)
                            html += 'Artist: ' + obj.tracks[track].artist + '<br>';

                        if (obj.tracks[track].album)
                            html += 'Album: ' + obj.tracks[track].album + '<br>';

                        html += '<div class="clearfix"></div>';


                    }

                $('#history').html(html);
            }
        }
    });

}

function myFormat(str, totalDigits) {
    if (totalDigits === undefined)
        totalDigits = 2;

    return ('00' + str).substr(-totalDigits);
}