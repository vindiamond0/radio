$(function()
{
    renderTree();

    fillPlaylist();
});

function renderTree()
{
    $("#treeData").html(markup);

    $("#tree")
        .fancytree({
            icons: false, // Display node icons.
            clickFolderMode: 2, // 1:activate, 2:expand, 3:activate and expand, 4:activate (dblclick expands)
            activate: function(event, data)
            {
                //console.log(data.node.key);
            },
            beforeSelect: function(event, data){
                // A node is about to be selected: prevent this for folders:
                /*if( data.node.isFolder() ){
                    return false;
                }*/
            },
            dblclick: function (event, data) {
                var node = data.node,
                    // Only for click and dblclick events:
                    // 'title' | 'prefix' | 'expander' | 'checkbox' | 'icon'
                    targetType = data.targetType;

                if (data.node.folder)
                {
                    if (prompt('Введи "yes", если понимаешь, что делаешь:') === "yes")
                        addFile(data.node.key);
                }
                else
                    addFile(data.node.key);

                // we could return false to prevent default handling, i.e. generating
                // activate, expand, or select events

            }
        });
}

function fillPlaylist()
{
    fetch('/ajax/getList', {
        method: 'post',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        }
    })
        .then(function (response) {
            return response.status >= 101 && response.status < 300
                ? Promise.resolve(response)
                : Promise.reject(new Error(response.statusText))
        })
        .then(function (res) {
            return res.json();
        })
        .then(function (obj) {
            if (!obj.success)
                return Promise.reject(new Error(obj.descr));

            let html = '', interrupt = 0;

            obj.list.forEach(function (file, key, array) {
                var lengthMin = Math.floor(file.Time / 60);
                var lengthSec = file.Time % 60;

                if (Number(obj.status.song) !== key)
                    html += '<br><span class="time">' + lengthMin + ':' + oo(lengthSec) + '</span> ' + file.basename;
                else
                {
                    var leftMin = Math.floor((file.Time - obj.status.elapsed) / 60);
                    var leftSec = Math.floor((file.Time - obj.status.elapsed) % 60);

                    html += '<br><button class="btn btn-light"><i class="fas fa-play"></i></button> ' + leftMin + ':' + oo(leftSec) + ' (<span class="time">' + lengthMin + ':' + oo(lengthSec) + '</span>) ' + file.basename;

                    interrupt = Math.floor((file.Time - obj.status.elapsed) * 1000);
                }
            });

            if (interrupt < 10000)
                interrupt = 10000;

            console.log('wait: ' + interrupt);

            $('#playList').html(html);

            setTimeout(fillPlaylist, interrupt);

            console.log('done: ' + interrupt);
        })
        .catch(function (error) {
            console.log('Request failed', error);
        });


    // $.ajax({
    //     type: 'POST',
    //     url: '/ajax/getList',
    //     success: function (data)
    //     {
    //         obj = data ? JSON.parse(data) : {};
    //
    //         if (obj && obj.success)
    //         {
    //             let html = '';
    //
    //             obj.list.forEach(function (file, key, array)
    //             {
    //                 var lengthMin = Math.floor(file.Time / 60);
    //                 var lengthSec = file.Time % 60;
    //                 var leftMin = Math.floor((file.Time - obj.status.elapsed) / 60);
    //                 var leftSec = Math.floor((file.Time - obj.status.elapsed) % 60);
    //
    //                 html += obj.status.song == key
    //                     ? ('<br><button class="btn btn-light"><i class="fas fa-play"></i></button> ' + leftMin + ':' + oo(leftSec) + ' (<span class="time">' + lengthMin + ':' + oo(lengthSec) + '</span>) ' + file.basename)
    //                     : ('<br><span class="time">' + lengthMin + ':' + oo(lengthSec) + '</span> ' + file.basename);
    //             });
    //
    //             $('#playList').html(html);
    //         }
    //     }
    // });
}

$('#play').click(function ()
{
    $.ajax({
        type: 'POST',
        url: '/ajax/play',
        success: function (data)
        {
            obj = data ? JSON.parse(data) : {};

            if (obj && obj.success)
            {
                $('#play').hide();
                $('#stop').show();
            }
        }
    });
});

$('#repeat').click(function ()
{
    $.ajax({
        type: 'POST',
        url: '/ajax/repeat',
        success: function (data)
        {
            obj = data ? JSON.parse(data) : {};

            if (obj && obj.success)
            {
                let btn = $('#repeat');

                if (obj.state)
                    btn.removeClass('btn-primary').addClass('btn-light');
                else
                    btn.removeClass('btn-light').addClass('btn-primary');
            }
        }
    });
});

$('#random').click(function ()
{
    $.ajax({
        type: 'POST',
        url: '/ajax/random',
        success: function (data)
        {
            obj = data ? JSON.parse(data) : {};

            if (obj && obj.success)
            {
                let btn = $('#random');

                if (obj.state)
                    btn.removeClass('btn-primary').addClass('btn-light');
                else
                    btn.removeClass('btn-light').addClass('btn-primary');
            }
        }
    });
});

$('#stop').click(function ()
{
    $.ajax({
        type: 'POST',
        url: '/ajax/stop',
        success: function (data)
        {
            obj = data ? JSON.parse(data) : {};

            if (obj && obj.success)
            {
                $('#stop').hide();
                $('#play').show();
            }
        }
    });
});

$('#trash').click(function ()
{
    if (prompt('Введи "del", если понимаешь, что делаешь:') === "del")
        $.ajax({
            type: 'POST',
            url: '/ajax/clearpl',
            success: function (data)
            {
                obj = data ? JSON.parse(data) : {};

                if (obj && obj.success)
                    fillPlaylist();
            }
        });
});

function addFile(keyId)
{
    $.ajax({
        type: 'POST',
        url: '/ajax/add',
        data: 'id=' + keyId,
        success: function (data)
        {
            obj = data ? JSON.parse(data) : {};

            if (obj && obj.success)
                fillPlaylist();
        }
    });
}

function oo(num)
{
    return num < 10
        ? "0" + num
        : num;
}

function switchpl(plkey) {
    console.log(plkey);
}