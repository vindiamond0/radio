FROM php:7.1-fpm

RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli

RUN pecl install xdebug
RUN docker-php-ext-enable xdebug
RUN docker-php-ext-install gettext && echo extension=gettext.so > /usr/local/etc/php/conf.d/gettext.ini

COPY conf /etc/php/7.1/fpm/conf.d/40-custom.ini
